object Exercise_1_2{
    def main(args:Array[String]){
        
        println("default params: " + finalSalary())
        println("user params: " + finalSalary(20000,0.5,50000))
        println("default incrementPercent: " + finalSalary(20000,5000))
        println("user params: " + finalSalary(50000,0.05,50000))
        println("default bonus:" + finalSalary(20000,0.5))
        println("default incrementPercent and bonus: " + finalSalary(30000))
        
    }
def finalSalary(salary:Int = 20000,incrementPercent:Double = 0.05,bonus:Int = 5000): Double ={
  var finalSalary = salary * ( 1 + incrementPercent) + bonus
  return finalSalary
}
}