import scala.collection.immutable.HashMap
class Employee(var empID: String,var empName: String,var location: String,var salary: Int){
    
    var loc = HashMap("BDC"->"Bangalore","CDC"->"Chennai","HDC"->"Hyderabad","Others"->"Unknown")
    
    def this(empID:String,empName:String,salary:Int){
        this(empID,empName,"HDC",salary)
    }
    
    
    def getEmpName(empID: String): String = {
        
        if (empID == "20203144") return empName else "Not Found"
    }
    
    def incrementSalary(empID: String,salIncPct: Double = 0.5): Double = {
        
        if (empID == "20203144") return (salary * ( 1 + salIncPct)) else 0
        
    }
    
    def getEmployeeloc(locID:String):Option[String]={
        
       var result = loc.get(locID)
       return result
    }
    
    
}

object Employee{
    
    def main(args:Array[String]){
        
        var e = new Employee("20203144","Srividya","BDC",25000)
        var e1 = new Employee("20203144","Srividya",50000)
        
        println("Emp Name: " + e.getEmpName(e.empID))
        println("increment salary: " + e.incrementSalary(e.empID))
        
        println("loc of emp " + e.getEmployeeloc(e.location))
        println("loc of emp " + e1.getEmployeeloc(e1.location))
        
        
        
    }
}
