class Employee(var empID: String,var empName: String,var location: String,var salary: Int){
    
    
    def this(empID:String,empName:String,salary:Int){
        this(empID,empName,"vskp",salary)
    }
    
    
    def getEmpName(empID: String): String = {
        
        if (empID == "20203144") return empName else "Not Found"
    }
    
    def incrementSalary(empID: String,salIncPct: Double = 0.5): Double = {
        
        if (empID == "20203144") return (salary * ( 1 + salIncPct)) else 0
        
    }
    
}

object Emp{
    def main(args:Array[String]){
        
        var e = new Employee("20203144","Srividya","hyd",25000)
        var e1 = new Employee("20203144","Srividya",50000)
        
        println("Emp Name: " + e.getEmpName(e.empID))
        println("increment salary: " + e.incrementSalary(e.empID))
        
    }
}
